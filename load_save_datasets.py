
from __future__ import print_function

import sys
import os
import time

import numpy as np
np.random.seed(1234) # for reproducibility?

# specifying the gpu to use
# import theano.sandbox.cuda
# theano.sandbox.cuda.use('gpu1') 
import theano
import theano.tensor as T

import lasagne

import cPickle as pickle
import gzip

from pylearn2.datasets.zca_dataset import ZCA_Dataset    
from pylearn2.utils import serial
from pylearn2.datasets.cifar100 import CIFAR100
from pylearn2.datasets.cifar10 import CIFAR10

from collections import OrderedDict

if __name__ == "__main__":
    
   
    
    dataset='cifar10'
    print('Loading CIFAR-100 dataset...')
   
    train_set=CIFAR10(which_set='train',start=0,stop=train_set_size)
    valid_set=CIFAR10(which_set='train',start=train_set_size,stop=50000)
    test_set=CIFAR10(which_set='test')

    try:
        os.makedirs('./datasets')
    except OSError:
        if not os.path.isdir('./datasets'):
            raise

    #below section will load pkl format dataset stored in dataset folder
    pkl_ldstart = time.time()
    print 'loading pkl format datasets'

    with open('./datasets/{}_train_x.pkl'.format(dataset),'wb') as ftrain_x:
        pickle.dump(train_set.X, ftrain_x)
    with open('./datasets/{}_train_y.pkl'.format(dataset),'wb') as ftrain_y:
        pickle.dump(train_set.y,ftrain_y)
    with open('./datasets/{}_valid_x.pkl'.format(dataset),'wb') as fvalid_x:
        pickle.dump(valid_set.X,fvalid_x)
    with open('./datasets/{}_valid_y.pkl'.format(dataset), 'wb') as fvalid_y:
        pickle.dump(valid_set.y,fvalid_y)
    with open('./datasets/{}_test_x.pkl'.format(dataset),'wb') as ftest_x:
        pickle.dump(test_set.X,ftest_x)
    with open('./datasets/{}_test_y.pkl'.format(dataset),'wb') as ftest_y:
        pickle.dump(test_set.y,ftest_y)

    pkl_ldtime = time.time() - pkl_ldstart
    print 'pkl loading time for {} datasets = {}s'.format(dataset, pkl_ldtime)

    #this section will save the dataset in npy format
    print 'saving in npy format'
    npysavesttime = time.time()

    np.save('./datasets/{}_train_x.npy'.format(dataset), train_set_x)
    np.save('./datasets/{}_train_y.npy'.format(dataset), train_set_y)
    np.save('./datasets/{}_valid_x.npy'.format(dataset), valid_set_x)
    np.save('./datasets/{}_valid_y.npy'.format(dataset), valid_set_y)
    np.save('./datasets/{}_test_x.npy'.format(dataset), test_set_x)
    np.save('./datasets/{}_test_y.npy'.format(dataset), test_set_y)

    npysavetime = time.time() -npysavesttime
    print 'npy format from numpy save time= {}'.format(npysavetime)

